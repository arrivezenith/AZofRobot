﻿using RBTofAZ.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBTofAZ.Data
{
    class UserInfo
    {
        /// <summary>
        /// 用户识别码
        /// </summary>
        public string UserID { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public Sex Sexure { get; set; }
        /// <summary>
        /// 年龄
        /// </summary>
        public int Age { set; get; }
        /// <summary>
        /// 上次活动时间
        /// </summary>
        public long LastActiveTime { get; set; }
        /// <summary>
        /// 消息状态
        /// </summary>
        public MessageStatus MsgStatus { get; set; }
        public UserInfo(string UserID)
        {
            this.UserID = UserID;
            this.MsgStatus = MessageStatus.GettingName;
            this.LastActiveTime = General.GetTimestamp13();
        }
        public enum Sex
        {
            Boy,
            Girl
        }
        public enum MessageStatus
        {
            Normal,
            GettingName,
            GettingSex,
            GettingAge,
            Study,
        }
        public override string ToString()
        {
            return Name;
        }
    }
}
