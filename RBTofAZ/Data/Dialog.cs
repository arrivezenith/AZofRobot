﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBTofAZ.Data
{
    class Dialog
    {
        public string UserID { get; set; }
        public string Recieved { get; set; }
        public string Response { get; set; }
        public long Time { get; set; }

        public Dialog(string user_ud, string rcv, string respons, long time)
        {
            UserID = user_ud;
            Recieved = rcv;
            Response = respons;
            Time = time;
        }
    }
}
