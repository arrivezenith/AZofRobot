﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using MySql.Data.MySqlClient;
using RBTofAZ.AI.Study;
using RBTofAZ.Net;

namespace RBTofAZ.Data
{
    class MysqlConnection
    {
        static MySqlConnection sqlCon;
        private MySqlDataReader reader = null;

        public MysqlConnection(string connstr)
        {
            sqlCon = new MySqlConnection(connstr);
            sqlCon.Open();
        }
        public MessageHistory GetMessageHistoryByID(string user_id)
        {
            
            MySqlCommand cmd = new MySqlCommand("select * from message where id='" + user_id + "'", sqlCon);
            var history = new MessageHistory(user_id);
            try
            {
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    history.Messages.Add(
                        new MessageHistory.Message
                    {
                        Accept = reader[2].ToString(),
                        Aply = reader[3].ToString(),
                        Time = Convert.ToInt64(reader[4])
                    });
                }
                return history;
            }
            catch(Exception ex)
            {
                General.Println("GetMessageHistoryByID:" + ex.Message);
                return null;
            }
            finally
            {
                reader.Close();
                sqlCon.Close();
            }
        }
        public List<string> SelectSentence(string cmd_s)
        {
            MySqlCommand cmd = new MySqlCommand(cmd_s, sqlCon);
            List<string> list_sencentes = new List<string>();
            try
            {
                reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        list_sencentes.Add(reader[2].ToString());
                    }
                    reader.Close();
                    return list_sencentes;
            }
            catch { return null; }
            finally
            {
                reader.Close();
                sqlCon.Close();
            }
        }
        public UserInfo SelectUser(string id)
        {
            MySqlCommand cmd = new MySqlCommand("select * from user where id='" + id + "'", sqlCon);
            try
            {
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    UserInfo user = new UserInfo(id);
                    user.Name = reader[2].ToString();
                    user.Age = (int)reader[3];
                    user.Sexure = (int)reader[4] == 1 ? UserInfo.Sex.Boy : UserInfo.Sex.Girl;
                    user.LastActiveTime = Convert.ToInt64(reader[5].ToString());
                    user.MsgStatus = UserInfo.MessageStatus.Normal;
                    reader.Close();
                    sqlCon.Close();
                    return user;
                }
                return null;
            }
            catch (Exception ex)
            {
                General.Println("SelectUser:" + ex.Message);
                return null;
            }
            finally
            {
                reader.Close();
                if(sqlCon.State== ConnectionState.Open)sqlCon.Close();
            }
        }
        public List<UserInfo> SelectUser()
        {
            var result = new List<UserInfo>();
            MySqlCommand cmd = new MySqlCommand("select * from user", sqlCon);
            try
            {
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    UserInfo user = new UserInfo(reader[1].ToString());
                    user.Name = reader[2].ToString();
                    user.Age = (int)reader[3];
                    user.Sexure = (int)reader[4] == 1 ? UserInfo.Sex.Boy : UserInfo.Sex.Girl;
                    user.LastActiveTime = Convert.ToInt64(reader[5].ToString());
                    user.MsgStatus = UserInfo.MessageStatus.Normal;
                    result.Add(user);
                }
                reader.Close();
                sqlCon.Close();
                return result;
            }
            catch (Exception ex)
            {
                General.Println("SelectUser:" + ex.Message);
                return null;
            }
            finally
            {
                reader.Close();
                if (sqlCon.State == ConnectionState.Open) sqlCon.Close();
            }
        }
        public void RefreshUser(UserInfo user)
        {
            var usr = SelectUser(user.UserID);
            if (usr == null)
            {
                string str_cmd = string.Format("insert into user(id,name,age,sex,time) values('{0}','{1}','{2}','{3}','{4}')", user.UserID, user.Name, user.Age, user.Sexure == UserInfo.Sex.Boy ? "1" : "0", user.LastActiveTime);
                Execute(str_cmd);
            }
            else
            {
                string str_cmd = string.Format("update user set time='{0}' where id='{1}'", user.LastActiveTime, user.UserID);
                Execute(str_cmd);
            }
        }
        public void DeleteUser(UserInfo user)
        {
            var usr = SelectUser(user.UserID);
            if (usr == null) return;
            string str_cmd = string.Format("delete from user where id='{0}'", user.UserID);
            Execute(str_cmd);
        }
        public void InsertMessageRecord(Dialog dialog)
        {
            string str_cmd = string.Format("insert into message(id,recieved,response,time) values('{0}','{1}','{2}','{3}')", dialog.UserID, dialog.Recieved, dialog.Response, dialog.Time);
            Execute(str_cmd);
        }
        public void InsertStudyedData(StudyDialog dialog)
        {
            string str_cmd = string.Format("insert into chat_s(keyword,sentence) values('{0}','{1}')", dialog.Question, dialog.Answer);
            Execute(str_cmd);
        }
        private void Execute(string cmd)
        {
            using (MySqlCommand sqlCommand = new MySqlCommand(cmd))
            {
                sqlCommand.Connection = sqlCon;
                if (sqlCon.State == ConnectionState.Closed) sqlCon.Open();
                sqlCommand.ExecuteNonQuery();
                sqlCon.Close();
            }
        }
    }
}
