﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBTofAZ.Data
{
    class MessageHistory
    {
        public string UserID { get; set; }
        private string user_name;
        public string UserName
        {
            get
            {
                if (user_name != null) return user_name;
                else
                {
                    user_name = UserManager.GetUser(UserID).Name;
                    return user_name;
                }
            }
        }
        public List<Message> Messages { get; set; }

        public MessageHistory(string user_id)
        {
            UserID = user_id;
            Messages = new List<Message>();
        }
        public struct Message
        {
            public string Accept { get; set; }
            public string Aply { get; set; }
            public long Time { get; set; }
        }

    }
}
