﻿using PanGu;
using PanGu.Match;
using RBTofAZ.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RBTofAZ.AI
{
    class RecievedStringHandler
    {
        private static string muid = "robot-az";
        private static string msgtype = "text";
        private static PanGu.Segment segment = new PanGu.Segment();
        private static MatchOptions options = new MatchOptions
        {
            ChineseNameIdentify = false,
            FrequencyFirst = false,
            MultiDimensionality = true,
            EnglishMultiDimensionality = false,
            FilterStopWords = true,
            IgnoreSpace = true,
            FilterEnglish = true,
            FilterNumeric = false,
            IgnoreCapital = true,
        };

        public static string GetSendBackString(string orinin_text)
        {
            string str_back = null;
            var words = segment.DoSegment(orinin_text, options);
            var keywords = new List<string>();
            var answers = new List<string>();
            keywords.Add(orinin_text);
            foreach(var word in words)
            {
                if (word.Frequency > 1000)
                    keywords.Add(word.Word);
            }
            foreach(var word in keywords)
            {
                var aswer = GetAnswer(word);
                if (aswer != null)
                    answers.Add(aswer);
            }
            if (answers.Count == 0) str_back = General.DontUnderstand;
            else str_back = answers[General.GetRandom(0, answers.Count)];

            str_back = str_back.Replace("[cqname]", "小A");
            str_back = str_back.Replace("[name]", "你");
            str_back = str_back.Replace("[enter]", ",");
            return str_back;
        }
        public static string CreateXmlMessageStructure(string user, string msg)
        {
            string fmt = "<xml> <ToUserName>{0}</ToUserName> <FromUserName>{1}</FromUserName> " +
                "<CreateTime>{2}</CreateTime> <MsgType>{3}</MsgType> <Content>{4}</Content> </xml>";
            return string.Format(fmt, user, muid, General.GetTimestamp13(), msgtype, msg);
        }
        public static string GetAnswer(string keyword)
        {
            var data_answers = General.mysqlConnection.SelectSentence("select * from chat where keyword like '%" + keyword + "%'");
            if (data_answers.Count == 0)
            {
                data_answers = General.mysqlConnection.SelectSentence("select * from chat_s where keyword like '%" + keyword + "%'");
            }
            if (data_answers.Count == 0) return null;
            return data_answers[General.GetRandom(0, data_answers.Count)];
        }
    }
}
