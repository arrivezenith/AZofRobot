﻿using RBTofAZ.AI.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBTofAZ.AI
{
    class MessageHandler
    {
        public static IMessageHandler Handler { get; private set; }
        private static List<IMessageHandler> messageHandlers = new List<IMessageHandler>();

        public static void Init()
        {
            messageHandlers.Add(new WeatherHandler());
            messageHandlers.Add(new HotWordsHandler());
            messageHandlers.Add(new NormalHandler());
        }
        public static string Handle(object sender, string msg)
        {
            string str_back = "";
            foreach(var handler in messageHandlers)
            {
                str_back = handler.Handle(sender, msg);
                if (handler.IsHandled())
                {
                    Handler = handler;
                    break;
                }
            }
            return str_back;
        }
    }
}
