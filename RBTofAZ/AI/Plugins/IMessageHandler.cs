﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBTofAZ.AI
{
    interface IMessageHandler
    {
        string Handle(object sender, string msg);
        bool IsHandled();
    }
}
