﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBTofAZ.AI.Plugins
{
    class SensorFilter
    {
        private static Dictionary<string, List<string>> SensorDic = new Dictionary<string, List<string>>();

        private static Dictionary<string, List<string>> GetSensorDic()
        {
            Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();
            FileStream fs = new FileStream("sensor.txt", FileMode.Open);
            StreamReader reader = new StreamReader(fs);
            string ln;
            var crtkey = "0";
            var crtlist = new List<string>();
            do
            {
                ln = reader.ReadLine();
                if (crtkey != ln[0].ToString())
                {
                    result.Add(crtkey, crtlist);
                    crtkey = ln[0].ToString();
                    crtlist = new List<string>();
                }
                if (!crtlist.Contains(ln))
                    crtlist.Add(ln);
            } while (ln != "EOF");
            reader.Close();
            return result;
        }

        public static void Init()
        {
            SensorDic = GetSensorDic();

        }

        public static bool Filter(string raw_str)
        {
            var result = true;
            var lenth = raw_str.Length;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < lenth; i++) 
            {
                if (SensorDic.ContainsKey(raw_str[i].ToString()))
                {
                    foreach(var word in SensorDic[raw_str[i].ToString()])
                    {
                        if (raw_str.Length - i < word.Length) continue;
                        var word_clip = raw_str.Substring(i, word.Length);
                        if (word == word_clip)
                        {
                            result = false;
                            return result;
                        }
                    }
                }
            }
            return result;
        }
    }
}
