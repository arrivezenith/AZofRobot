﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBTofAZ.AI.Study
{
    class StudyDialog
    {
        private string User { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        
        public bool Finished
        {
            get
            {
                if (Answer != null) return true;
                else return false;
            }
        }
        public StudyDialog(string user_id,string question)
        {
            this.User = user_id;
            this.Question = question;
        }
        public void SetAnswer(string answer)
        {
            if (Answer == null)
                this.Answer = answer;
            else
                throw new Exception("属性值冲突.");
        }
        public bool March(string user_id)
        {
            if (User == user_id) return true;
            else return false;
        }
    }
}
