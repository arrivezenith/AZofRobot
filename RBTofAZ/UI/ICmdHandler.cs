﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBTofAZ.UI
{
    interface ICmdHandler
    {
        void Handle(string cmd);
    }
}
