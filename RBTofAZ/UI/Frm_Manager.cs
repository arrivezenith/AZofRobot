﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RBTofAZ.UI
{
    public partial class Frm_Manager : Form
    {
        public Frm_Manager()
        {
            InitializeComponent();
        }
        private void Frm_Manager_Load(object sender, EventArgs e)
        {
            FillUserList();
            InitBasicInfo();
            SelectAllInformation();
        }
        private void listBox1_Click(object sender, EventArgs e)
        {
            SelectInformation();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            SelectAllInformation();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            SelectMsg();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            SelectAllMsg();
        }
        private void button4_Click(object sender, EventArgs e)
        {
            GetWordCloud();
        }
    }
}
