﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBTofAZ.UI
{
    class AdminCmdHandler
    {
        private static List<ICmdHandler> Handlers = new List<ICmdHandler>();
        public static void Init()
        {
            Handlers.Add(new UI.Handlers.MessageHistory());
            Handlers.Add(new UI.Handlers.ShowForm());
        }
        public static void Handle(string cmd)
        {
            foreach(var handler in Handlers)
            {
                handler.Handle(cmd);
            }
        }
    }
}
