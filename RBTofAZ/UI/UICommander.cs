﻿using MySql.Data.MySqlClient;
using PanGu.Match;
using RBTofAZ.Data;
using RBTofAZ.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using WordCloudGen = WordCloud.WordCloud;

namespace RBTofAZ.UI
{
    public partial class Frm_Manager
    {
        List<UserInfo> Users = new List<UserInfo>();
        List<UserInfo> ActiveUsers = new List<UserInfo>();
        void FillUserList()
        {
            MySqlConnection sqlCon = new MySqlConnection("server=localhost;User Id=root;password=whuttokenaz;Database=robot;SslMode = none;Charset=utf8;");
            sqlCon.Open();
            var cmd_s = "select * from user";
            MySqlCommand cmd = new MySqlCommand(cmd_s, sqlCon);
            MySqlDataReader reader = null;
            try
            {
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var user = new UserInfo(reader[1].ToString())
                    {
                        Name = reader[2].ToString(),
                        Age = Convert.ToInt32(reader[3]),
                        Sexure = reader[4].ToString() == "1" ? UserInfo.Sex.Boy : UserInfo.Sex.Girl,
                        LastActiveTime = Convert.ToInt64(reader[5])
                    };
                    Users.Add(user);
                }
                listBox1.DataSource = Users;
                reader.Close();
                label5.Text = "用户数量:" + Users.Count;
                return;
            }
            catch { return; }
            finally
            {
                reader.Close();
                sqlCon.Close();
            }
        }
        void SelectInformation()
        {
            var crtusr = (UserInfo)listBox1.SelectedItem;
            richTextBox1.Clear();
            richTextBox1.AppendText("用户ID:" + crtusr.UserID + "\n");
            richTextBox1.AppendText("用户名:" + crtusr.Name + "\n");
            richTextBox1.AppendText("性别:" + (crtusr.Sexure == UserInfo.Sex.Boy ? "男" : "女") + "\n");
            richTextBox1.AppendText("年龄:" + crtusr.Age + "\n");
            richTextBox1.AppendText("上次活跃:" + General.GetTimeStandard(crtusr.LastActiveTime.ToString()).ToString("yyyy-MM-dd HH:mm:ss") + "\n");
        }
        void SelectAllInformation()
        {
            richTextBox1.Clear();
            foreach (var crtusr in Users)
            {
                richTextBox1.AppendText("用户ID:" + crtusr.UserID + "\n");
                richTextBox1.AppendText("用户名:" + crtusr.Name + "\n");
                richTextBox1.AppendText("性别:" + (crtusr.Sexure == UserInfo.Sex.Boy ? "男" : "女") + "\n");
                richTextBox1.AppendText("年龄:" + crtusr.Age + "\n");
                richTextBox1.AppendText("上次活跃:" + General.GetTimeStandard(crtusr.LastActiveTime.ToString()).ToString("yyyy-MM-dd HH:mm:ss") + "\n");
                richTextBox1.AppendText("\n");
                if (General.GetTimestamp13() - crtusr.LastActiveTime < 10 * 60 * 1000) ActiveUsers.Add(crtusr);
            }
            label6.Text += ActiveUsers.Count;
        }
        void SelectMsg()
        {
            var crtusr = (UserInfo)listBox1.SelectedItem;
            var history = General.mysqlConnection.GetMessageHistoryByID(crtusr.UserID);
            foreach (var msg in history.Messages)
            {
                richTextBox1.AppendText(General.GetTimeStandard(msg.Time.ToString()).ToString("[MM-dd HH:mm:ss]") + crtusr.Name + ":" + msg.Accept + "\n");
                richTextBox1.AppendText(General.GetTimeStandard(msg.Time.ToString()).ToString("[MM-dd HH:mm:ss]") + "小A:" + msg.Aply + "\n");
            }
        }
        void SelectAllMsg()
        {
            richTextBox1.Clear();
            foreach (var crtusr in Users)
            {
                var history = General.mysqlConnection.GetMessageHistoryByID(crtusr.UserID);
                foreach (var msg in history.Messages)
                {
                    richTextBox1.AppendText(General.GetTimeStandard(msg.Time.ToString()).ToString("[MM-dd HH:mm:ss]") + crtusr.Name + ":" + msg.Accept + "\n");
                    richTextBox1.AppendText(General.GetTimeStandard(msg.Time.ToString()).ToString("[MM-dd HH:mm:ss]") + "小A:" + msg.Aply + "\n");
                }
                richTextBox1.AppendText(Environment.NewLine);
            }
        }
        void GetWordCloud()
        {
            MatchOptions options = new MatchOptions
            {
                 
                ChineseNameIdentify = false,
                 
                FrequencyFirst = false,
                MultiDimensionality = true,
                EnglishMultiDimensionality = false,
                FilterStopWords = true,
                IgnoreSpace = true,
                FilterEnglish = true,
                FilterNumeric = false,
                IgnoreCapital = true,
            };
            PanGu.Segment segment = new PanGu.Segment();
            Dictionary<string, int> words = new Dictionary<string, int>();
            foreach (var crtusr in Users)
            {
                var history = General.mysqlConnection.GetMessageHistoryByID(crtusr.UserID);
                foreach (var msg in history.Messages)
                {
                    var wds = segment.DoSegment(msg.Aply, options);
                    foreach(var wd in wds)
                    {
                        if (wd.Frequency < 200 || wd.Word.Length < 1) continue;
                        if (words.ContainsKey(wd.Word))
                        {
                            words[wd.Word]++;
                        }
                        else
                        {
                            words.Add(wd.Word, 1);
                        }
                    }
                    wds = segment.DoSegment(msg.Accept, options);
                    foreach (var wd in wds)
                    {
                        if (wd.Frequency < 200 || wd.Word.Length < 1) continue;
                        if (words.ContainsKey(wd.Word))
                        {
                            words[wd.Word]++;
                        }
                        else
                        {
                            words.Add(wd.Word, 1);
                        }
                    }
                }
            }
            //var dicSort = from objDic in words orderby objDic.Value descending select objDic;
            WordCloudGen wordCloud = new WordCloudGen(1000, 600);
            Thread thread = new Thread(delegate ()
            {
                Form form = new Form()
                {
                    Size = new System.Drawing.Size(1000, 600),
                    Text = "消息词云"
                };
                using (var wc = wordCloud.Draw(words.Keys.ToList(), words.Values.ToList()))
                {
                    form.BackgroundImage = wc;
                    form.ShowDialog();
                }
            });
            richTextBox1.Clear();
            richTextBox1.AppendText("词云绘制中，请耐心等待...");
            thread.Start();
        }
        void InitBasicInfo()
        {
            int getcount(string dataset_name)
            {
                int n = 0;
                MySqlConnection sqlCon = new MySqlConnection("server=localhost;User Id=root;password=whuttokenaz;Database=robot;SslMode = none;Charset=utf8;");
                sqlCon.Open();
                var cmd_s = "select * from " + dataset_name;
                MySqlCommand cmd = new MySqlCommand(cmd_s, sqlCon);
                try
                {
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        n++;
                    }
                    reader.Close();
                    return n;
                }
                catch
                {
                    return n;
                }
                finally
                {
                    sqlCon.Close();
                }
            }
            label2.Text += getcount("chat");
            label3.Text += getcount("chat_s");
        }
    } 
}
