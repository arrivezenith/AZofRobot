﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RBTofAZ.Net
{
    class General
    {
        private static Random r = new Random();
        public static string Token = "whuttokenaz1807";
        public static string Appid = "wx0e5fdcc2028c818e";
        public static string DontUnderstand = "小A不明白你在说什么呢...";
        public static Data.MysqlConnection mysqlConnection
        {
            get
            {
                return new Data.MysqlConnection("server=localhost;User Id=root;password=whuttokenaz;Database=robot;SslMode = none;Charset=utf8;");
            }
            
        }
        /// <summary>
        /// 获取13位时间戳
        /// </summary>
        /// <returns></returns>
        public static long GetTimestamp13()
        {
            TimeSpan ts = DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1);
            return (long)ts.TotalMilliseconds; //精确到毫秒
        }
        public static DateTime GetTimeStandard(string timeStamp)
        {
            DateTime time = DateTime.Now;
            if (string.IsNullOrEmpty(timeStamp))
            {
                return time;
            }
            try
            {
                DateTime dtStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
                long lTime = long.Parse(timeStamp + "0000");
                TimeSpan toNow = new TimeSpan(lTime);
                time = dtStart.Add(toNow);
            }
            catch (Exception ex)
            {
                
            }
            return time;
        }
        public static int GetRandom(int l,int u)
        {
            return r.Next(l, u);
        }
        public static void Println(string text)
        {
            Console.WriteLine(DateTime.Now.ToString("[MM-dd HH:mm:ss]") + text);
        }
    }
}
